#####################
##### grid search  ###
######################
library(readr)
library(ggplot2)
library(plyr)
library(dplyr)
library(h2o)
library(parallel)
library(smoother)
library(zoo)

h2o.init()

setwd("~/Tasks/UW/Amira")
test.names <- c("ColdTest","EngineKnockLeanData", "EngineKnockStoichData", 
                "FuelTrimSystemLeanData", "FuelTrimSystemRichData", "HotTest",
                "MisfireCoilDisconnectCyl4Data", "MisfireIgnitionAdvanceCyl4Data", "MisfireIgnitionAdvanceCyl4LeanData", 
                "MisfireIgnitionRetardCyl4Data", "MisfireLeanCyl4Data", "MisfireRichCyl4Data")

for (j in 3:length(test.names)){
  if ((j==1)|(j==6)){
    next
  }

  test.name <- test.names[j]
  train = read_csv('df_train.csv')
  test = read_csv(paste0('df_faulty_',j,'.csv'))
  train = subset(train, select = -c(X1) )
  test = subset(test, select = -c(X1) )
  
  print(paste0(">>>>>>>>>>>>It is test: ",j,". which is about: ",test.name))
  
  train.time.index = which(colnames(train) == "time")
  test.time.index = which(colnames(test)=="time")
  hidden_par <- list( c(30,10,30), c(30,20,30),c(20,10,20)) 
  l1_par <- c(1e-3,1e-8)
  l2_par <- c(1e-3,1e-8)
  hyperp <- list(hidden=hidden_par, l1=l1_par, l2=l2_par, activation=c("Tanh")) 
  grid = h2o.grid("deeplearning", hyper_params=hyperp, x=colnames(train)[-train.time.index],
                  training_frame=as.h2o(train[,-train.time.index]),reproducible = T, 
                  seed = 1234, epochs = 200, autoencoder=TRUE,do_hyper_params_check = TRUE)
  
  summary(grid)
  model_ids = grid@model_ids
  models = lapply(model_ids,function(id){ h2o.getModel(id)})
  
  for(i in 1:length(models)){
    print(paste0("model",i,": MSE =", models[[i]]@model$training_metrics@metrics$MSE))
  }
  
  j=8
  imp.vars = models[[j]]@model$variable_importances
  write.csv(imp.vars, file =paste0("imp_features/imp_features_",test.name,".csv"))
  print(imp.vars)
  
  save(grid,file=paste0("Result/grid_",test.name,".RData"))
  err = list()
  smth = list()
  for(i in 1:length(models)){
    err[[i]] = as.data.frame(h2o.anomaly(models[[i]], as.h2o(test[,-test.time.index])))
    smth[[i]] = smth(err[[i]]$Reconstruction.MSE, method='ema')
    smth[[i]] = smth.gaussian(err[[i]]$Reconstruction.MSE, window = 0.05,tails = T)
    smth[[i]] = na.approx(smth[[i]])
  }
  save(err,file=paste0("Result/err_for_all_mdls_",test.name,".RData"))
  save(smth,file=paste0("rec_error_for_all_mdls/",test.name,".RData"))
  ################################################################
  ##########plotting all models and saving pdf file###############
  color = grDevices::colors()[grep('gr(a|e)y', grDevices::colors(), invert = T)]
  smth.col=sample(color, length(smth))
  
  rootDirName='/home/amir/Tasks/UW/Amira'
  pdf(file.path(rootDirName,"plots",paste0("rec_err_",test.name,"_all_mdls.pdf")))
  plot(data.matrix(test[,test.time.index]),smth[[1]],type="l",ylim = c(0.01,0.3),ylab="Smoothed reconstruction error",xlab="Time",main = test.name)
  legend("topleft",legend=1:length(smth),col=smth.col,pch=1,bty="n")
  for(i in 2:length(smth)){
    points(data.matrix(test[,test.time.index]), smth[[i]],col=smth.col[i],type="l")
  }
  dev.off()
}


#----------------------------------------------------------------------------
#>>>>>>>>>>>Loading the data and saving plots as pdf and anomaly period<<<<<<<<<<<<<<<<<<

anom.time.df <- data.frame(anomaly_start=rep(0,12), anomaly_end=rep(0,12),row.names = test.names)
#loading the file
i=12
test.name <- test.names[i]
load(file =paste0("rec_error_for_all_mdls/",test.name,".RData"))
test.name <- test.names[i]
train = read_csv('df_train.csv')
test = read_csv(paste0('df_faulty_',i,'.csv'))
train = subset(train, select = -c(X1) )
test = subset(test, select = -c(X1) )
print(paste0(">>>>>>>>>>>>It is test: ",i,". which is about: ",test.name))
train.time.index = which(colnames(train) == "time")
test.time.index = which(colnames(test)=="time")

plot(data.matrix(test[,test.time.index]),smth[[1]],type="l",ylim = c(0.01,0.2),ylab="Smoothed reconstruction error",xlab="Time",main = test.name)
legend("topleft",legend=1:length(smth),col=smth.col,pch=1,bty="n")
for(i in 2:length(smth)){
  points(data.matrix(test[,test.time.index]), smth[[i]],col=smth.col[i],type="l")
}
j=2
# imp.vars = models[[j]]@model$variable_importances
# write.csv(imp.vars, file =paste0("imp_features/imp_features_",test.name,".csv"))
# print(imp.vars)
dev.off()
#plot the best model rec err 
plot(test$time, smth[[j]], ylab="rec_err",xlab="Time",main = test.name)
grid (NULL,NULL, lty = 6, col = "cornsilk2") 

anomaly.start = 114
anomaly.end = 243

anom.plot.df <- data.frame(time=test$time, err=smth[[j]])
anom.plot.df <- anom.plot.df %>% mutate(flag = ifelse(((time>anomaly.start)&(time<anomaly.end)),2,1))

dev.off()
pdf(file.path(rootDirName,"plots",paste0("anomaly_rgn_",test.name,".pdf")))
plot(test$time, smth[[j]], col = anom.plot.df$flag, ylab="rec_err",xlab="Time",main = test.name)
grid (NULL,NULL, lty = 6, col = "cornsilk2") 
dev.off()

anom.time.df[test.name,'anomaly_start']=anomaly.start
anom.time.df[test.name,'anomaly_end']=anomaly.end





#finding anomaly region

#smth[[i]] = smth.gaussian(err[[i]]$Reconstruction.MSE, window = 0.01,tails = T)
sm <-smth[[i]]
df.smth.index <- data.frame(... = index=1:length(sm) , err=sm)
df.smth.index <- scale(df.smth.index)
cl <- kmeans(x = df.smth.index, centers = 3)

plot(test$time, sm, col = cl$cluster, ylab="rec_err",xlab="Time")
points(cl$centers, col = 1:3, pch = 8)
print(cl$centers)

df.smth.cl <- data.frame(index=1:length(sm), err=sm, cluster=cl$cluster)
df.anom <- subset(df.smth.cl, subset = df.smth.cl[,3]==3)
anomaly.start = test[min(df.anom$index),]$time
anomaly.end = test[max(df.anom$index),]$time

pdf(file.path(rootDirName,"plots",paste0("anomaly_rgn_",test.name,".pdf")))
plot(test$time, sm, col = cl$cluster, ylab="rec_err",xlab="Time",main = test.name)
dev.off()

##############################################################
######################feature selection#######################
library(tictoc)
setwd("~/Tasks/UW/Amira")
anom <- list(c(0,0),c(16,227),c(115,160),c(69,400),c(60,350),c(0,0),c(50,165),c(58,172),c(103,196),c(48,218),c(60,224),c(44,278))
i <- 3
# for (i in 3:length(test.names)){
#   if ((i==1)|(i==6)|(i==8)|(i==7))
#   {
#     next
#   }
test.name <- test.name <- test.names[i]
print(paste0(">>>>>>>>>>>>Feature Selection for test: ",i,", ",test.name," starts at ",anom[[i]][1]," ends at ",anom[[i]][2]))
test = read_csv(paste0('df_faulty_',i,'.csv'))
test = subset(test, select = -c(X1) )
#test.time.index = which(colnames(test)=="time")
#test=test[,-test.time.index]

anomaly.start = anom[[i]][1]
anomaly.end = anom[[i]][2]

setwd("~/Tasks/fcaotr/explore/amira/")
source("feature_selection_on_pseudolabels.R")

tic()
dataset.x.y = prep.x.y(test,anomaly.start, anomaly.end)
dataset.x = dataset.x.y[["x"]]
dataset.y = dataset.x.y[["y"]]
nested.cv.test = lasso_feature_selection_on_one_dataset(dataset.x,dataset.y)
dir.create(test.name)
file.copy("nested_cv_output", test.name, recursive=TRUE)
unlink("nested_cv_output",recursive=TRUE)

# # SVM-RFE
# dir.create(paste0(test.name,"/svm_rfe"))
# test.svm.rfe = svm_rfe_on_one_dataset(dataset.x, dataset.y, "test/svm_rfe")

source("repeat_lasso_percentile.R")
test.agg.out = repeat_lasso_on_one_dataset(dataset.x, dataset.y, test.name, test.name, 100)
toc()
# }

################################################################################
##############Amira and intersection kind of anomaly identification#############

# anomaly.mse.threshold = 0.3
# indices = list()
# for(i in 1:length(smth)){
#   indices[[i]] = which(smth[[i]]>anomaly.mse.threshold)
# }
# 
# smth.selected = list(smth[[1]],smth[[2]],smth[[3]],smth[[4]],smth[[5]],smth[[8]])
# inter.indices = intersect(indices[[1]],indices[[2]])
# for(i in 3:length(smth.selected)){
#   inter.indices = intersect(inter.indices,indices[[i]])
# }
# anomaly.start = test[min(inter.indices),]$time
# anomaly.end = test[max(inter.indices),]$time

