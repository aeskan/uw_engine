#!/usr/bin/env Rscript

# tool to source local file
source_local <- function(fname){
  argv <- commandArgs(trailingOnly = FALSE)
  base_dir <- dirname(substring(argv[grep("--file=", argv)], 8))
  if (length(base_dir)) { source(paste(base_dir, fname, sep="/")) }
  else {cat("ERROR: Could not source, running in a repl.\n")}
}

# pre-requisite: 
# 1. datalist 
  # source cleanup.R and run main() to get datalist
  # source_local("cleanup.R")
  # datalist = main()

#2. 
library(readr)
library(ggplot2)
library(plyr)
library(dplyr)
library(h2o)
library(parallel)

# The second step is to apply semisupervised anomaly detection method such as autoencoder on the GM data

# Function: train and test autoencoder with given training and test data
# Detail: Seed is set to 1234 in the h2o.deeplearning function
train_test<- function(train.hex, test.hex,featureset, save_folder, maxruntime_secs = 3600){
  
  #train.dl = h2o.deeplearning(x = featureset, training_frame = train.hex,
  #                            autoencoder = TRUE,
  #                            reproducible = T,
  #                            seed = 1234,
  #                            hidden = c(30, 5, 30), epochs = 200, activation = "Tanh")
  
  # Replace the above function with one that returns best model over a set of parameters with a stopping criteria.
  
  # Search a random subset of these hyper-parmameters.
  our_search_criteria = list(strategy = "RandomDiscrete", 
                         max_runtime_secs = maxruntime_secs, 
                         max_models = 100, 
                         stopping_metric = "AUTO", 
                         stopping_tolerance = 0.00001, 
                         stopping_rounds = 5, 
                         seed = 123456)
  
  #Combinations of inner/outer, hidden layer node count
  inoutnodes = seq(round(0.5*length(featureset)),length(featureset),1)
  middlenodes = seq(round(0.5*length(featureset)),round(0.75*length(featureset)),1)
  
  combinations = expand.grid(inoutnodes,middlenodes)
  
  hiddenlist =  unlist(apply(X = cbind(combinations,combinations$Var1),MARGIN = 1,FUN = list),recursive = FALSE)
  
  #Search space of hyperparam
  our_search_hyperparam = list(hidden = hiddenlist, 
                               epochs = list(c(100),c(200)),
                               activation = "Tanh")
  
  h2o.grid(algorithm = "deeplearning",grid_id = "thegrid", x = featureset, training_frame = train.hex,
                              autoencoder = TRUE, reproducible = T, seed = 1234,
                              hyper_params = our_search_hyperparam,
                              search_criteria = our_search_criteria)
  
  #Sort the grid based on your interest attribute (mse/rmse for autoencoder)
  griddata_rmse = h2o.getGrid("thegrid",sort_by = "rmse",decreasing = FALSE)
  
  #Id of the best model 
  train.dl = h2o.getModel(griddata_rmse@model_ids[[1]])
  
  dir.create(file.path(getwd(), save_folder), showWarnings = FALSE)
  
  save_path=paste("./",save_folder, sep = "")
  
  model_path = h2o.saveModel(object=train.dl, path=save_path, force=TRUE)
  
  train.anon = h2o.anomaly(train.dl, train.hex, per_feature=T)
  
  train.p = h2o.predict(train.dl, train.hex)
  
  train.p = as.data.frame(train.p)
  
  save(train.p, file = paste0(save_path, "/train_p.RData"))
  
  train.err <- as.data.frame(train.anon)
  
  save(train.err, file = paste(save_path, "/train_error.RData",sep = ""))
  
  trainrowerr = rowMeans(train.err, na.rm = TRUE)
 
   # test
  test.anon = h2o.anomaly(train.dl, test.hex, per_feature=T)
  
  test.p = h2o.predict(train.dl, test.hex)
  
  test.p = as.data.frame(test.p)
  
  save(test.p, file = paste0(save_path, "/test_p.RData"))
  
  test.err <- as.data.frame(test.anon)
  
  save(test.err, file = paste(save_path, "/test_error.RData",sep = ""))
  
  testrowerr = rowMeans(test.err, na.rm = TRUE)
  
  return(list(trainrowerr, testrowerr))
}

# Function: train on fca1 and test on fca2 and the other way around
# Detail: save the plots to pdf
aerunner <- function(datalist){

  h2o.init()
  
  featureset = names(datalist[[1]])[!names(datalist[[1]]) %in% c("time")]
  
  for(i in 1:length(datalist)){
    train.hex = as.h2o(datalist[[i]])
    test.hex = as.h2o(datalist[[3-i]])
      
    save_folder = paste("train_on_fca0",i,"test_on_fca0",3-i,sep = "")
    trt_list = train_test(train.hex, test.hex, featureset, save_folder)
    
    pdf(paste("./",save_folder,"/",save_folder,"_recon_err.pdf",sep = ""))
    plot(datalist[[i]]$time, trt_list[[1]], main='Train Reconstruction Error')
    plot(datalist[[3-i]]$time, trt_list[[2]], main='Test Reconstruction Error')
    dev.off()
    
  }
  h2o.shutdown()
}

# Function: train autoencoder only on the "normal" part.
# Detail: train on ONE of fca1 fca2, and test the model on THE OTHER based on given train_data_id
#         with the specified time range EXCLUDED
aerunner_timesect<- function(datalist,train_data_id,time_start,time_end){
  h2o.init()
  
  featureset = names(datalist[[train_data_id]])[!names(datalist[[train_data_id]]) %in% c("time")]
  
  time = datalist[[train_data_id]]$time
  flg = (time > time_start) & (time < time_end)
 
  train.hex = as.h2o(datalist[[train_data_id]][flg,])
  test.hex = as.h2o(datalist[[3-train_data_id]])
  
  save_folder = paste("train_on_fca0",train_data_id,"_anomaly_removed_","test_on_fca0",3-train_data_id,sep = "")

  trt_list = train_test(train.hex, test.hex, featureset, save_folder)
  
  pdf(paste("./",save_folder,"/",save_folder,"_recon_err.pdf",sep = ""))
  plot(datalist[[train_data_id]]$time[flg], trt_list[[1]], main='Train Reconstruction Error')
  plot(datalist[[3-train_data_id]]$time, trt_list[[2]], main='Test Reconstruction Error')
  dev.off()
    
  h2o.shutdown()
}

# Function: train and test on the same dataset
# Detail: i=1 means train on FCA1 and test on FCA1
trt_same<-function(datalist,i){
  h2o.init()
  featureset = names(datalist[[i]])[!names(datalist[[i]]) %in% c("time")]
  train.hex = as.h2o(datalist[[i]])
  test.hex = train.hex
  save_folder = paste("train_on_fca0",i,"test_on_fca0",i,sep = "")
  trt_list = train_test(train.hex, test.hex, featureset, save_folder)
  pdf(paste("./",save_folder,"/",save_folder,"_recon_err.pdf",sep = ""))
  plot(datalist[[i]]$time, trt_list[[1]], main='Train Reconstruction Error')
  plot(datalist[[i]]$time, trt_list[[2]], main='Test Reconstruction Error')
  dev.off()
  h2o.shutdown()
}


# examples of use the above functions
#aerunner(datalist)
#aerunner_timesect(datalist,2,40000,58000)
#trt_same(datalist,2)

# Function: check the mean reconstruction err by features and list the most important 20
# Detail: load the result according to train, test dataid and train/test error
var_imp <- function(train_dataid,test_dataid,loadfile,datalist){
  load_folder = paste("train_on_fca0",train_dataid,"test_on_fca0",test_dataid,sep = "")
  load_path = paste("./",load_folder,"/",loadfile,sep = "")
  recon_err = load(load_path)

  if(loadfile=="train_error.RData"){
    err = colMeans(train.err, na.rm = TRUE)
    
  }else{
    err = colMeans(test.err, na.rm = TRUE)
  }
  var_name = names(datalist[[1]])[!names(datalist[[1]])%in%c("time")]
  result = cbind(var_name,err)%>%as.data.frame %>% arrange(desc(err))
  
  return(result)
}

#>>>>>>>>>>>>>>>>>>>>>>>  MAIN  <<<<<<<<<<<<<<<<<<<<
test.names <- c("ColdTest","EngineKnockLeanData", "EngineKnockStoichData", 
                "FuelTrimSystemLeanData", "FuelTrimSystemRichData", "HotTest",
                "MisfireCoilDisconnectCyl4Data", "MisfireIgnitionAdvanceCyl4Data", "MisfireIgnitionAdvanceCyl4LeanData", 
                "MisfireIgnitionRetardCyl4Data", "MisfireLeanCyl4Data", "MisfireRichCyl4Data")

setwd("/home/amir/Tasks/UW/Mahmoud")
rootDirName <- '/home/amir/Tasks/UW/Mahmoud'

#for (j in 1:length(test.names)){
j <- 12
if (j==12){
  if ((j!=1)&(j!=6)){
    
    test.name <- test.names[j]
    print(paste0(">>>>>>>>>>>>It is test: ",j,". which is about: ",test.name))
    setwd("/home/amir/Tasks/UW/Amira")
    train = read_csv('df_train.csv')
    test = read_csv(paste0('df_faulty_',j,'.csv'))
    setwd("/home/amir/Tasks/UW/Mahmoud")
    train = subset(train, select = -c(X1) )
    test = subset(test, select = -c(X1) )
    train.time.index = which(colnames(train) == "time")
    test.time.index = which(colnames(test)=="time")
    
    datalist= list(train[,-train.time.index],test[,-test.time.index])
    
    h2o.init()
    featureset = names(datalist[[1]])[!names(datalist[[1]]) %in% c("time")]
    train.hex = as.h2o(datalist[[1]])
    test.hex = as.h2o(datalist[[2]])
    dir.create(test.name)
    save_folder = file.path(test.name)
    trt_list = train_test(train.hex, test.hex, featureset, save_folder)
    
    datalist= list(train,test)
    pdf(file.path(rootDirName,save_folder,"recon_err_train_and_test.pdf"))
    plot(datalist[[1]]$time, trt_list[[1]], main='Train Reconstruction Error')
    plot(datalist[[2]]$time, trt_list[[2]], main='Test Reconstruction Error')
    dev.off()
    
    load(file.path(save_folder,"test_error.RData"))
    err <- colMeans(test.err, na.rm <- TRUE)
    var_name <- names(err)[!names(err)%in%c("time")]
    var_name <- gsub("reconstr_","", var_name)
    var_name <- gsub(".SE","",var_name)
    var_importance <- cbind(var_name, as.numeric(as.character(err))) %>%as.data.frame
    var_importance <- var_importance %>% arrange(desc(err))
    save(var_importance,file=file.path(save_folder,"tuning_var_importance.RData"))
    write.table(var_importance,file.path(save_folder,"tuning_var_importance.csv"),sep=",",quote=FALSE,row.names=FALSE)
    h2o.shutdown(prompt = F)
  }
}
# imp.vars = models[[j]]@model$variable_importances
# write.csv(imp.vars, file =paste0("imp_features/imp_features_",test.name,".csv"))
# print(imp.vars)