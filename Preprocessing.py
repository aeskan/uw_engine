
# coding: utf-8

# In[1]:


import sys
import pickle
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
get_ipython().run_line_magic('matplotlib', 'inline')


# In[2]:


df = pd.read_csv('Data/engine-failures.csv')


# In[3]:


print(np.unique(df['scenario'].values))
df.head()


# In[4]:


test=[]
ind_before=0
for ind_next in df[df['time']==0].index:
    if ind_next!=0:
        test.append(df.iloc[ind_before:ind_next,:])
    ind_before=ind_next
test.append(df.iloc[ind_next:,:])


# In[5]:


for i in range(len(test)):
    test[i]= test[i].sort_values(by=['time'])
    test[i]= test[i].interpolate()


# In[4]:


# #interpolation
# df2=df.interpolate()


# In[6]:


#spliting dataset by scenarios
# test=[]
# ind_before=0
# for ind_next in df[df2['time']==0].index:
#     if ind_next!=0:
#         test.append(df2.iloc[ind_before:ind_next,:])
#     ind_before=ind_next
# test.append(df2.iloc[ind_next:,:])


# In[7]:


# #removing the short period of time at the end of each test that time drops and goes up again
# for i in range(len(test)):
#     for index in range(test[i].index.min(),test[i].index.max()-1):
#         if test[i].loc[index]['time']>test[i].loc[index+1]['time']:
#             bad_index=index
#             break
#     if bad_index!=0:
#         test[i]=test[i].loc[:bad_index,:]
#     bad_index=0


# In[6]:


#creating training data (merging baseline, hot test, cold test)
df_train=pd.concat([test[0],test[1],test[6]],axis=0)
print(np.unique(df_train['scenario'].values))
df_train.drop(['file','scenario','status'], axis=1, inplace=True)
df_train.index=np.arange(1,df_train.shape[0]+1)
df_train


# In[11]:


df_train=df_train.rename(columns = {'time':'t'})


# In[12]:


df_train


# In[13]:


#saving orginal data (not normalized)
for i in range(len(test)):
    if (i!=0)&(i!=1)&(i!=6):
        temp1=test[i].iloc[:,0:1] # time
        temp2=test[i].iloc[:,1:(39-3)] # variables
        df_faulty=pd.concat([temp1,temp2],axis=1)
        name_faulty='df_faulty_nn_'+str(i)+'.csv'
        df_faulty.to_csv('Data/Pre/'+name_faulty)
temp1=df_train.iloc[:,0:1]
temp2=df_train.iloc[:,1:(39-3)]
df_train=pd.concat([temp1,temp2],axis=1)
df_train.to_csv('Data/Pre/df_train.csv')


# In[15]:


#normalizing the data
avg1=df_train.drop(['t'], axis=1).mean()
std1=df_train.drop(['t'], axis=1).std()
# avg1=df_train.drop(['time'], axis=1).mean()
# std1=df_train.drop(['time'], axis=1).std()
for i in range(len(test)):
    if (i!=0)&(i!=1)&(i!=6):
        temp1=test[i].iloc[:,0:1] # time
        temp2=test[i].iloc[:,1:(39-3)] # variables
        temp2=(temp2-avg1)/std1
        df_faulty=pd.concat([temp1,temp2],axis=1)
        name_faulty='df_faulty_'+str(i)+'.csv'
        df_faulty.to_csv('Amira/'+name_faulty)
temp1=df_train.iloc[:,0:1]
temp2=df_train.iloc[:,1:(39-3)]
temp2=(temp2-avg1)/std1
df_train=pd.concat([temp1,temp2],axis=1)
df_train.to_csv('df_train.csv')


# In[44]:


df_faulty


# In[38]:


plt.figure(figsize=(20,10))
plt.plot(df_train['time'])
#plt.xticks(np.arange(0, 85, 1))
#plt.ylim([0,0.5])
#plt.xlim([0,30])
#plt.yscale('log')
#plt.legend(loc='upper left',prop={'size': 20})
#plt.xlabel('Orders', fontsize=15)
plt.grid(True)
plt.show()


# In[4]:


get_ipython().system(' pwd')

